package dev.abhiroopsantra.movies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        return new ResponseEntity<List<Movie>>(movieService.allMovies(), HttpStatus.OK);
    }

    @GetMapping("/{imdbId}")
    public ResponseEntity<Movie> getMovieById(@PathVariable String imdbId) {
        try {
            Optional<Movie> movie = movieService.movieByImdbId(imdbId);

            return movie
                    .map(value -> new ResponseEntity<Movie>(value, HttpStatus.OK))
                    .orElseGet(() -> new ResponseEntity<Movie>((Movie) null, HttpStatus.NOT_FOUND));
        } catch (Exception ex) {
            return new ResponseEntity<Movie>((Movie) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
