package dev.abhiroopsantra.movies;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovieRepository extends MongoRepository<Movie, ObjectId> {
    // it is assumed that imdbId for a movie will be unique
    Optional<Movie> findMovieByImdbId(String imdbId);
}
